/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Aditya Singh
 */
public class UberDetailsHistory {
    
   private ArrayList<UberDetails> uberDetailsHistory;
   
   public UberDetailsHistory()
   {
       
      uberDetailsHistory = new  ArrayList<UberDetails>(); 
   }

    public ArrayList<UberDetails> getUberDetailsHistory() {
        return uberDetailsHistory;
    }

    public void setUberDetailsHistory(ArrayList<UberDetails> uberDetailsHistory) {
        this.uberDetailsHistory = uberDetailsHistory;
    }
   
    
   public UberDetails addDetails() 
   {
       UberDetails ud = new  UberDetails();
       uberDetailsHistory.add(ud);
       return ud;
   }
   
   public void deleteDetails(UberDetails u)
   {
      uberDetailsHistory.remove(u);
   }
    
}
