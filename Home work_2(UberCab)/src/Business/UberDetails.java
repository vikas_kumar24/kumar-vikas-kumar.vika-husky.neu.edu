/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Aditya Singh
 */
public class UberDetails {
       private String manufacturer;
    private String dateofManufacture;
    private int    seats;
    private String serialNumber;
    private int    modelNum;
    private boolean isAvailable;
    private String catUpdate;
    private String certificateDate;
    private String cityAvailability;

    public String getCertificateDate() {
        return certificateDate;
    }

    public void setCertificateDate(String certificateDate) {
        this.certificateDate = certificateDate;
    }

    public String getCityAvailability() {
        return cityAvailability;
    }

    public void setCityAvailability(String cityAvailability) {
        this.cityAvailability = cityAvailability;
    }

    public String getCatUpdate() {
        return catUpdate;
    }

    public void setCatUpdate(String catUpdate) {
        this.catUpdate = catUpdate;
    }

    
    public boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDateofManufacture() {
        return dateofManufacture;
    }

    public void setDateofManufacture(String dateofManufacture) {
        this.dateofManufacture = dateofManufacture;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getModelNum() {
        return modelNum;
    }

    public void setModelNum(int modelNum) {
        this.modelNum = modelNum;
    }
    
    @Override
    public String toString()
    {
        return this.manufacturer;
    }

  
    
}
