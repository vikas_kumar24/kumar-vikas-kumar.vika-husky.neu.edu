/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.UberDetailsHistory;
import Business.UberDetails;
import javax.swing.JOptionPane;

/**
 *
 * @author Aditya Singh
 */
public class EnterCarJPanel extends javax.swing.JPanel {

    /**
     * Creates new form EnterCarJPanel
     */
    
    private UberDetailsHistory udh; 
    public EnterCarJPanel(UberDetailsHistory udh) {
         initComponents();
         this.udh= udh;
         
        //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblManu = new javax.swing.JLabel();
        txtManufacturer = new javax.swing.JTextField();
        lblManu1 = new javax.swing.JLabel();
        txtDateofManufacture = new javax.swing.JTextField();
        lblManu2 = new javax.swing.JLabel();
        txtSeats = new javax.swing.JTextField();
        lblManu3 = new javax.swing.JLabel();
        txtModelNum = new javax.swing.JTextField();
        lblManu4 = new javax.swing.JLabel();
        txtSerialNum = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        availableCheckBox = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        lblCatUpdate = new javax.swing.JLabel();
        txtCatUpdate = new javax.swing.JTextField();
        lblMinCert = new javax.swing.JLabel();
        txtMainCert = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        lblCity = new javax.swing.JLabel();

        lblManu.setText("Manufacturer");

        txtManufacturer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtManufacturerActionPerformed(evt);
            }
        });

        lblManu1.setText("Date of Manufacture");

        txtDateofManufacture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDateofManufactureActionPerformed(evt);
            }
        });

        lblManu2.setText("Seats");

        txtSeats.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSeatsActionPerformed(evt);
            }
        });

        lblManu3.setText("Model #");

        txtModelNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtModelNumActionPerformed(evt);
            }
        });

        lblManu4.setText("Serial #");

        txtSerialNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSerialNumActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Enter Car Details");

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        availableCheckBox.setText("avaialablity");

        jLabel2.setText("Cab Availab");

        lblCatUpdate.setText("Date of Catlog Update");

        txtCatUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCatUpdateActionPerformed(evt);
            }
        });

        lblMinCert.setText("Maintanenece Certificate");

        txtMainCert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMainCertActionPerformed(evt);
            }
        });

        lblCity.setText("City Availablity");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(120, 120, 120)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(276, 276, 276)
                        .addComponent(btnSave))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblManu1, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblManu2)
                                        .addComponent(lblManu))
                                    .addComponent(lblManu3))
                                .addComponent(lblManu4))
                            .addComponent(jLabel2)
                            .addComponent(lblCatUpdate)
                            .addComponent(lblMinCert)
                            .addComponent(lblCity))
                        .addGap(70, 70, 70)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(availableCheckBox)
                            .addComponent(txtSerialNum, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                            .addComponent(txtModelNum)
                            .addComponent(txtSeats)
                            .addComponent(txtDateofManufacture)
                            .addComponent(txtManufacturer)
                            .addComponent(txtCatUpdate)
                            .addComponent(txtMainCert)
                            .addComponent(txtCity))))
                .addContainerGap(229, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel1)
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtManufacturer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblManu))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDateofManufacture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblManu1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSeats, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblManu2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtModelNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblManu3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSerialNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblManu4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(availableCheckBox)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblCatUpdate)
                        .addGap(18, 18, 18)
                        .addComponent(lblMinCert)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblCity)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtCatUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtMainCert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                        .addComponent(btnSave)
                        .addGap(21, 21, 21))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtManufacturerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtManufacturerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtManufacturerActionPerformed

    private void txtDateofManufactureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDateofManufactureActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDateofManufactureActionPerformed

    private void txtSeatsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSeatsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSeatsActionPerformed

    private void txtModelNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtModelNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtModelNumActionPerformed

    private void txtSerialNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSerialNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSerialNumActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        String manufacturer = txtManufacturer.getText();
       String dateofManufacture = txtDateofManufacture.getText();
       int    Seats = Integer.parseInt(txtSeats.getText());
       int    modelNum = Integer.parseInt(txtModelNum.getText());
       String serialNumber = txtSerialNum.getText();
       String catUpdate = txtCatUpdate.getText();
       String certificateDate = txtMainCert.getText();
       String cityAvailability = txtCity.getText();

       
       UberDetails u = udh.addDetails();
       u.setManufacturer(manufacturer);
       u.setDateofManufacture(dateofManufacture);
       u.setSeats(Seats);
       u.setModelNum(modelNum);
       u.setSerialNumber(serialNumber);
       u.setCatUpdate(catUpdate);
       u.setCertificateDate(certificateDate);
       u.setCityAvailability(cityAvailability);
       
       if(availableCheckBox.isSelected()){
           u.setIsAvailable(true);
       }
       else{
           u.setIsAvailable(false);
       }
       
       
       JOptionPane.showMessageDialog(null,"Data was loaded successfully"); 
       
       txtManufacturer.setText("");
       txtDateofManufacture.setText("");
       txtSeats.setText("");
       txtModelNum.setText("");
       txtSerialNum.setText("");
       txtCatUpdate.setText("");
       
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void txtCatUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCatUpdateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCatUpdateActionPerformed

    private void txtMainCertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMainCertActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMainCertActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox availableCheckBox;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblCatUpdate;
    private javax.swing.JLabel lblCity;
    private javax.swing.JLabel lblManu;
    private javax.swing.JLabel lblManu1;
    private javax.swing.JLabel lblManu2;
    private javax.swing.JLabel lblManu3;
    private javax.swing.JLabel lblManu4;
    private javax.swing.JLabel lblMinCert;
    private javax.swing.JTextField txtCatUpdate;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDateofManufacture;
    private javax.swing.JTextField txtMainCert;
    private javax.swing.JTextField txtManufacturer;
    private javax.swing.JTextField txtModelNum;
    private javax.swing.JTextField txtSeats;
    private javax.swing.JTextField txtSerialNum;
    // End of variables declaration//GEN-END:variables
}
