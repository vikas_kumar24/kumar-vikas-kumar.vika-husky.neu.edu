/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author Aditya Singh
 */
public class TravelAgentDirectory {
    private ArrayList<TravelAgent> travelagentList;
    
    public TravelAgentDirectory(){
    ArrayList<TravelAgent>travelagentList;
    this.travelagentList = new ArrayList<TravelAgent>();
    
}  

    public TravelAgentDirectory(JPanel userProcessContainer, TravelAgentDirectory travelAgentDirectory) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<TravelAgent> getTravelagentList() {
        return travelagentList;
    }

    public void setTravelagentList(ArrayList<TravelAgent> travelagentList) {
        this.travelagentList = travelagentList;
    }
    
    public TravelAgent searchTravelAgent(String flightNumber){
        for (TravelAgent travelagent: travelagentList){
            if (travelagent.getFlightNumber().equals(flightNumber)){
                return travelagent;
            }
        }
        return null;
    }
    
    public void bookFlight(TravelAgent travelagent){
        travelagentList.remove(travelagent);
    }
    
    
    
    
    
}
