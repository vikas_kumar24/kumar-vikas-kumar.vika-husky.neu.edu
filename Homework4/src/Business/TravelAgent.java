/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author Aditya Singh
 */
public class TravelAgent {
    private String flightNumber;
    private String departure;
    private String arrivalLocation;
    private String timeofflight;
    private Date createdOn;
    
    public TravelAgent(){
        this.createdOn = new Date();
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
    
    

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(String arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public String getTimeofflight() {
        return timeofflight;
    }

    public void setTimeofflight(String timeofflight) {
        this.timeofflight = timeofflight;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    @Override
    public String toString(){
        return this.flightNumber;
    }
    
  
}
