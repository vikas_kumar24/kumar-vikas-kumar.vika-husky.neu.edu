/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Aditya Singh
 */
public class AirlinerDirectory {
    private ArrayList<Airliner> airlinerList;

public AirlinerDirectory(){
    ArrayList<Airliner>airlinerList;
    this.airlinerList = new ArrayList<Airliner>();
    
}    
    

    public ArrayList<Airliner> getAirlinerList() {
        return airlinerList;
    }

    public void setAirliner(ArrayList<Airliner> airlinerList) {
        this.airlinerList = airlinerList;
    }
    
    public Airliner addAirliner(){
        Airliner airliner = new Airliner   ();
        airlinerList.add(airliner);
        return airliner;
    }
        
       
    
    public Airliner searchAirliner(String flightNumber){
        for (Airliner airliner: airlinerList){
            if (airliner.getFlightNumber().equals(flightNumber)){
                return airliner;
            }
        }
        return null;
    }
        
         public void cancelFlight(Airliner airliner){
        airlinerList.remove(airliner);
    }
    }
    
   
    


    